
function init_compose() {
    $("#compose, .compose-close").click(function () {
        $(".compose").slideToggle()
    })
}
init_compose();
$(document).ready(function () {
    $(".collapse-link").on("click", function () {
        var a = $(this).closest(".x_panel"),
            b = $(this).find("i"),
            c = a.find(".x_content");
        a.attr("style") ? c.slideToggle(200, function () {
            a.removeAttr("style")
        }) : (c.slideToggle(200), a.css("height", "auto")), b.toggleClass("fa-chevron-up fa-chevron-down")
    }), $(".close-link").click(function () {
        var a = $(this).closest(".x_panel");
        a.remove()
    })
}), $(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip({
        container: "body"
    })
}), $(document).ready(function () {
    $("input.flat")[0] && $(document).ready(function () {
        $("input.flat").iCheck({
            checkboxClass: "icheckbox_flat-green",
            radioClass: "iradio_flat-green"
        })
    })
}), $("table input").on("ifChecked", function () {
    checkState = "", $(this).parent().parent().parent().addClass("selected"), countChecked()
}), $("table input").on("ifUnchecked", function () {
    checkState = "", $(this).parent().parent().parent().removeClass("selected"), countChecked()
});
var checkState = "";
$(".bulk_action input").on("ifChecked", function () {
    checkState = "", $(this).parent().parent().parent().addClass("selected"), countChecked()
}), $(".bulk_action input").on("ifUnchecked", function () {
    checkState = "", $(this).parent().parent().parent().removeClass("selected"), countChecked()
}), $(".bulk_action input#check-all").on("ifChecked", function () {
    checkState = "all", countChecked()
}), $(".bulk_action input#check-all").on("ifUnchecked", function () {
    checkState = "none", countChecked()
}), $(document).ready(function () {
    $(".expand").on("click", function () {
        $(this).next().slideToggle(200), $expand = $(this).find(">:first-child"), "+" == $expand.text() ? $expand.text("-") : $expand.text("+")
    })
}), "undefined" != typeof NProgress && ($(document).ready(function () {
    NProgress.start()
}), $(window).load(function () {
    NProgress.done()
}));
var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function (a) {
    var c, d, b = a instanceof this.constructor ? a : $(a.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type);
    originalLeave.call(this, a), a.currentTarget && (c = $(a.currentTarget).siblings(".popover"), d = b.timeout, c.one("mouseenter", function () {
        clearTimeout(d), c.one("mouseleave", function () {
            $.fn.popover.Constructor.prototype.leave.call(b, b)
        })
    }))
}, $("body").popover({
    selector: "[data-popover]",
    trigger: "click hover",
    delay: {
        show: 50,
        hide: 400
    }
}), $(document).ready(function () {
});
function initSearchText() {
    var item = document.getElementsByClassName("item-scroll-listbox");
    var searchtext = document.getElementById('search-listbox').value;
    for (var i = 0; i < item.length; i++) {
        if (!item[i].getElementsByTagName('h3')[0].innerText.toUpperCase().includes(searchtext.toUpperCase())) {
            item[i].classList.add('hide');
        } else {
            item[i].classList.remove('hide');
        }
    }
}
function initFlip() {
    $(document).on("click", ".item-scroll-listbox", function () {
        $(document.getElementsByClassName('flip-custom')).toggleClass('hover');
    });
}
initFlip();

function reload(d1) {
    //$(document.getElementsByClassName('flip-custom')).toggleClass('hover');
}

try {
    function initTree() {
        var datatree = [
            {
                id: 1,
                text: 'Các dự thảo luật kinh tế',
                parent: '#',
                state: {
                    opened: true,
                }
            },
            {
                id: 2,
                text: 'Dự thảo luật doanh nghiệp sửa đổi',
                parent: 1,
                state: {
                    opened: true,
                }
            },
            {
                id: 3,
                text: 'Dự thảo luật đầu tư công',
                parent: 1,
                state: {
                    opened: true,
                }
            },
            {
                id: 4,
                text: 'Dự thảo luật cạnh tranh',
                parent: 1,
                state: {
                    opened: true,
                }
            },
            {
                id: 5,
                text: 'Dự thảo luật cổ phần hóa',
                parent: 4,
                state: {
                    opened: true,
                }
            },
            {
                id: 6,
                text: 'Các dự thảo luật kinh tế',
                parent: '#',
                state: {
                    opened: true,
                }
            },
            {
                id: 7,
                text: 'Dự thảo luật doanh nghiệp sửa đổi',
                parent: 6,
                state: {
                    opened: true,
                }
            },
            {
                id: 8,
                text: 'Dự thảo luật đầu tư công',
                parent: 6,
                state: {
                    opened: true,
                }
            },
            {
                id: 9,
                text: 'Dự thảo luật cạnh tranh',
                parent: 6,
                state: {
                    opened: true,
                }
            },
            {
                id: 10,
                text: 'Dự thảo luật cổ phần hóa',
                parent: 9,
                state: {
                    opened: true,
                }
            },
        ];
        $('#treeview61')
            .on('changed.jstree', function (e, data) {
                reload(data);
            })
            .jstree({
                'core': {
                    'data': datatree,
                },
                'checkbox': {
                    three_state: false
                },
                //'plugins': ["checkbox"]
            });
    }
    initTree();

    var Treeview = {
        init: function () {
            $("#m_tree_1").jstree({
                core: {
                    themes: {
                        responsive: !1
                    }
                },
                types: {
                    default: {
                        icon: "fa fa-folder"
                    },
                    file: {
                        icon: "fa fa-file"
                    }
                },
                plugins: ["types"]
            }), $("#m_tree_2").jstree({
                core: {
                    themes: {
                        responsive: !1
                    }
                },
                types: {
                    default: {
                        icon: "fa fa-folder m--font-warning"
                    },
                    file: {
                        icon: "fa fa-file  m--font-warning"
                    }
                },
                plugins: ["types"]
            }), $("#m_tree_2").on("select_node.jstree", function (e, t) {
                var n = $("#" + t.selected).find("a");
                if ("#" != n.attr("href") && "javascript:;" != n.attr("href") && "" != n.attr("href")) return "_blank" == n.attr("target") && (n.attr("href").target = "_blank"), document.location.href = n.attr("href"), !1
            }), $("#m_tree_3").jstree({
                plugins: ["wholerow", "checkbox", "types"],
                core: {
                    themes: {
                        responsive: !1
                    },
                    data: [{
                        text: "Dự thảo luật kinh tế",
                        children: [{
                            text: "Dự thảo luật thuế sửa đổi",
                            state: {
                                selected: !0
                            }
                        }, {
                            text: "Luật đầu tư công",
                            icon: "fa fa-warning m--font-danger"
                        }, {
                            text: "Luật doanh nghiệp",
                            icon: "fa fa-folder m--font-default",
                            state: {
                                opened: !0
                            },
                            children: ["Dự thảo"]
                        }, {
                            text: "Luật Đơn vị hành chính kinh tế đặc biệt",
                            icon: "fa fa-warning m--font-waring"
                        }, {
                            text: "Luật thuế tài sản",
                            icon: "fa fa-check m--font-success",
                            state: {
                                disabled: !0
                            }
                        }]
                    }, "Luật cạnh tranh sửa đổi"]
                },
                types: {
                    default: {
                        icon: "fa fa-folder m--font-warning"
                    },
                    file: {
                        icon: "fa fa-file  m--font-warning"
                    }
                }, plugins: ["contextmenu", "state", "types", "dnd"]
            }), $("#m_tree_4").jstree({
                core: {
                    themes: {
                        responsive: !1
                    },
                    check_callback: !0,
                    data: [{
                        text: "Báo cáo giám sát",
                        children: [{
                            text: "Giám sát cổ phần hóa DN nhà nước",
                            state: {
                                selected: !0
                            }
                        }, {
                            text: "Quản lý và sử dụng vốn",
                            icon: "fa fa-warning m--font-danger"
                        }, {
                            text: "Tài sản",
                            icon: "fa fa-folder m--font-success",
                            state: {
                                opened: !0
                            },
                            children: [{
                                text: "Giám sát tài sản công",
                                icon: "fa fa-file m--font-waring"
                            }]
                        }, {
                            text: "Báo cáo giám sát sử dụng vốn ngấn sách",
                            icon: "fa fa-warning m--font-waring"
                        }, {
                            text: "Giám sát chi ngân sách",
                            icon: "fa fa-check m--font-success",
                            state: {
                                disabled: !0
                            }
                        }, {
                            text: "Giám sát thu ngân sách",
                            icon: "fa fa-folder m--font-danger",
                            children: [{
                                text: "Năm 2018",
                                icon: "fa fa-file m--font-waring"
                            }, {
                                text: "Năm 2017",
                                icon: "fa fa-file m--font-success"
                            }, {
                                text: "Năm 2016",
                                icon: "fa fa-file m--font-default"
                            }, {
                                text: "Năm 2015",
                                icon: "fa fa-file m--font-danger"
                            }, {
                                text: "Năm 2014",
                                icon: "fa fa-file m--font-info"
                            }]
                        }]
                    }, "Các báo cáo khác"]
                },
                types: {
                    default: {
                        icon: "fa fa-folder m--font-brand"
                    },
                    file: {
                        icon: "fa fa-file  m--font-brand"
                    }
                },
                state: {
                    key: "demo2"
                },
                plugins: ["contextmenu", "state", "types"]
            }),


                $("#m_tree_demo").jstree({
                    core: {
                        themes: {
                            responsive: !1
                        },
                        check_callback: !0,
                        data: [{
                            text: "Các dự thảo luật kinh tế",
                            children: [{
                                text: "Dự thảo luật doanh nghiệp sửa đổi",
                                state: {
                                    selected: !0
                                }
                            }, {
                                text: "Dự thảo luật đầu tư công",
                                icon: "fa fa-warning m--font-danger"
                            }, {
                                text: "Dự thảo luật cạnh tranh",
                                icon: "fa fa-folder m--font-success",
                                state: {
                                    opened: !0
                                },
                                children: [{
                                    text: "Dự thảo luật cổ phần hóa",
                                    icon: "fa fa-file m--font-waring"
                                }]
                            }, {
                                text: "Tài sản công",
                                icon: "fa fa-warning m--font-waring"
                            }, {
                                text: "Quản lý vốn",
                                icon: "fa fa-check m--font-success",
                                state: {
                                    disabled: !0
                                }
                            }, {
                                text: "Dự thảo luật",
                                icon: "fa fa-folder m--font-danger",
                                children: [{
                                    text: "Dự thảo mới",
                                    icon: "fa fa-file m--font-waring"
                                }, {
                                    text: "Dự thảo đã thông qua",
                                    icon: "fa fa-file m--font-success"
                                }, {
                                    text: "Dự thảo chưa thông qua",
                                    icon: "fa fa-file m--font-default"
                                }, {
                                    text: "Dự thảo cần theo dõi",
                                    icon: "fa fa-file m--font-danger"
                                }, {
                                    text: "Dự thảo cần bổ sung",
                                    icon: "fa fa-file m--font-info"
                                }]
                            }]
                        }, "Các tài liệu khác liên quan dự thảo"]
                    },
                    types: {
                        default: {
                            icon: "fa fa-folder m--font-brand"
                        },
                        file: {
                            icon: "fa fa-file  m--font-brand"
                        }
                    },
                    state: {
                        key: "demo2"
                    },
                    plugins: ["contextmenu", "state", "types", "dnd"]
                }),


                $("#m_tree_demo_1").jstree({
                    core: {
                        themes: {
                            responsive: !1
                        },
                        check_callback: !0,
                        data: [{
                            text: "Hồ sơ Dự thảo Luật kinh tế",
                            children: [{
                                text: "Luật doanh nghiệp",
                                state: {
                                    selected: !0
                                }
                            }, {
                                text: "Dự thảo luật đầu tư công",
                                icon: "fa fa-warning m--font-danger"
                            }, {
                                text: "Dự thảo luật cạnh tranh",
                                icon: "fa fa-folder m--font-success",
                                state: {
                                    opened: !0
                                },
                                children: [{
                                    text: "Dự thảo luật cổ phần hóa",
                                    icon: "fa fa-file m--font-waring"
                                }]
                            }, {
                                text: "Tài sản công",
                                icon: "fa fa-warning m--font-waring"
                            }, {
                                text: "Quản lý vốn",
                                icon: "fa fa-check m--font-success",
                                state: {
                                    disabled: !0
                                }
                            }, {
                                text: "Dự thảo luật",
                                icon: "fa fa-folder m--font-danger",
                                children: [{
                                    text: "Dự thảo mới",
                                    icon: "fa fa-file m--font-waring"
                                }, {
                                    text: "Dự thảo đã thông qua",
                                    icon: "fa fa-file m--font-success"
                                }, {
                                    text: "Dự thảo chưa thông qua",
                                    icon: "fa fa-file m--font-default"
                                }, {
                                    text: "Dự thảo cần theo dõi",
                                    icon: "fa fa-file m--font-danger"
                                }, {
                                    text: "Dự thảo cần bổ sung",
                                    icon: "fa fa-file m--font-info"
                                }]
                            }]
                        }, "Các tài liệu khác liên quan dự thảo"]
                    },
                    types: {
                        default: {
                            icon: "fa fa-folder m--font-brand"
                        },
                        file: {
                            icon: "fa fa-file  m--font-brand"
                        }
                    },
                    state: {
                        key: "demo2"
                    },
                    plugins: ["contextmenu", "state", "types", "dnd"]
                }),


                $("#m_tree_demo_2").jstree({
                    core: {
                        themes: {
                            responsive: !1
                        },
                        check_callback: !0,
                        data: [{
                            text: "Hồ sơ Dự thảo Luật kinh tế",
                            children: [{
                                text: "Luật doanh nghiệp",
                                state: {
                                    selected: !0
                                }
                            }, {
                                text: "Dự thảo luật đầu tư công",
                                icon: "fa fa-warning m--font-danger"
                            }, {
                                text: "Dự thảo luật cạnh tranh",
                                icon: "fa fa-folder m--font-success",
                                state: {
                                    opened: !0
                                },
                                children: [{
                                    text: "Dự thảo luật cổ phần hóa",
                                    icon: "fa fa-file m--font-waring"
                                }]
                            }, {
                                text: "Tài sản công",
                                icon: "fa fa-warning m--font-waring"
                            }, {
                                text: "Quản lý vốn",
                                icon: "fa fa-check m--font-success",
                                state: {
                                    disabled: !0
                                }
                            }, {
                                text: "Dự thảo luật",
                                icon: "fa fa-folder m--font-danger",
                                children: [{
                                    text: "Dự thảo mới",
                                    icon: "fa fa-file m--font-waring"
                                }, {
                                    text: "Dự thảo đã thông qua",
                                    icon: "fa fa-file m--font-success"
                                }, {
                                    text: "Dự thảo chưa thông qua",
                                    icon: "fa fa-file m--font-default"
                                }, {
                                    text: "Dự thảo cần theo dõi",
                                    icon: "fa fa-file m--font-danger"
                                }, {
                                    text: "Dự thảo cần bổ sung",
                                    icon: "fa fa-file m--font-info"
                                }]
                            }]
                        }, "Các tài liệu khác liên quan dự thảo"]
                    },
                    types: {
                        default: {
                            icon: "fa fa-folder m--font-brand"
                        },
                        file: {
                            icon: "fa fa-file  m--font-brand"
                        }
                    },
                    state: {
                        key: "demo2"
                    },
                    plugins: ["contextmenu", "state", "types", "dnd"]
                }),




                $("#m_tree_demo_3").jstree({
                    core: {
                        themes: {
                            responsive: !1
                        },
                        check_callback: !0,
                        data: [{
                            text: "Hồ sơ Dự thảo Luật kinh tế",
                            children: [{
                                text: "Luật doanh nghiệp",
                                state: {
                                    selected: !0
                                }
                            }, {
                                text: "Dự thảo luật đầu tư công",
                                icon: "fa fa-warning m--font-danger"
                            }, {
                                text: "Dự thảo luật cạnh tranh",
                                icon: "fa fa-folder m--font-success",
                                state: {
                                    opened: !0
                                },
                                children: [{
                                    text: "Dự thảo luật cổ phần hóa",
                                    icon: "fa fa-file m--font-waring"
                                }]
                            }, {
                                text: "Tài sản công",
                                icon: "fa fa-warning m--font-waring"
                            }, {
                                text: "Quản lý vốn",
                                icon: "fa fa-check m--font-success",
                                state: {
                                    disabled: !0
                                }
                            }, {
                                text: "Dự thảo luật",
                                icon: "fa fa-folder m--font-danger",
                                children: [{
                                    text: "Dự thảo mới",
                                    icon: "fa fa-file m--font-waring"
                                }, {
                                    text: "Dự thảo đã thông qua",
                                    icon: "fa fa-file m--font-success"
                                }, {
                                    text: "Dự thảo chưa thông qua",
                                    icon: "fa fa-file m--font-default"
                                }, {
                                    text: "Dự thảo cần theo dõi",
                                    icon: "fa fa-file m--font-danger"
                                }, {
                                    text: "Dự thảo cần bổ sung",
                                    icon: "fa fa-file m--font-info"
                                }]
                            }]
                        }, "Các tài liệu khác liên quan dự thảo"]
                    },
                    types: {
                        default: {
                            icon: "fa fa-folder m--font-brand"
                        },
                        file: {
                            icon: "fa fa-file  m--font-brand"
                        }
                    },
                    state: {
                        key: "demo2"
                    },
                    plugins: ["contextmenu", "state", "types", "dnd"]
                }),


                $("#m_tree_demo_4").jstree({
                    core: {
                        themes: {
                            responsive: !1
                        },
                        check_callback: !0,
                        data: [{
                            text: "Hồ sơ Dự thảo Luật kinh tế",
                            children: [{
                                text: "Luật doanh nghiệp",
                                state: {
                                    selected: !0
                                }
                            }, {
                                text: "Dự thảo luật đầu tư công",
                                icon: "fa fa-warning m--font-danger"
                            }, {
                                text: "Dự thảo luật cạnh tranh",
                                icon: "fa fa-folder m--font-success",
                                state: {
                                    opened: !0
                                },
                                children: [{
                                    text: "Dự thảo luật cổ phần hóa",
                                    icon: "fa fa-file m--font-waring"
                                }]
                            }, {
                                text: "Tài sản công",
                                icon: "fa fa-warning m--font-waring"
                            }, {
                                text: "Quản lý vốn",
                                icon: "fa fa-check m--font-success",
                                state: {
                                    disabled: !0
                                }
                            }, {
                                text: "Dự thảo luật",
                                icon: "fa fa-folder m--font-danger",
                                children: [{
                                    text: "Dự thảo mới",
                                    icon: "fa fa-file m--font-waring"
                                }, {
                                    text: "Dự thảo đã thông qua",
                                    icon: "fa fa-file m--font-success"
                                }, {
                                    text: "Dự thảo chưa thông qua",
                                    icon: "fa fa-file m--font-default"
                                }, {
                                    text: "Dự thảo cần theo dõi",
                                    icon: "fa fa-file m--font-danger"
                                }, {
                                    text: "Dự thảo cần bổ sung",
                                    icon: "fa fa-file m--font-info"
                                }]
                            }]
                        }, "Các tài liệu khác liên quan dự thảo"]
                    },
                    types: {
                        default: {
                            icon: "fa fa-folder m--font-brand"
                        },
                        file: {
                            icon: "fa fa-file  m--font-brand"
                        }
                    },
                    state: {
                        key: "demo2"
                    },
                    plugins: ["contextmenu", "state", "types", "dnd"]
                }),



                $("#m_tree_5").jstree({
                    core: {
                        themes: {
                            responsive: !1
                        },
                        check_callback: !0,
                        data: [{
                            text: "Hồ sơ các vấn đề quan trọng",
                            children: [{
                                text: "Lấy ý kiến hoàn thiện luật cạnh tranh",
                                state: {
                                    selected: !0
                                }
                            }, {
                                text: "",
                                icon: "fa fa-warning m--font-danger"
                            }, {
                                text: "Luật kinh doanh bất động sản",
                                icon: "fa fa-folder m--font-success",
                                state: {
                                    opened: !0
                                },
                                children: [{
                                    text: "Thẩm tra sơ bộ luật phát triển đô thị",
                                    icon: "fa fa-file m--font-waring"
                                }]
                            }, {
                                text: "Một số điều luật các tổ chức tín dụng",
                                icon: "fa fa-warning m--font-waring"
                            }, {
                                text: "Luật đấu giá",
                                icon: "fa fa-check m--font-success",
                                state: {
                                    disabled: !0
                                }
                            }, {
                                text: "Sub Nodes",
                                icon: "fa fa-folder m--font-danger",
                                children: [{
                                    text: "Vấn đề quan trọng 1",
                                    icon: "fa fa-file m--font-waring"
                                }, {
                                    text: "Vấn đề quan trọng 2",
                                    icon: "fa fa-file m--font-success"
                                }, {
                                    text: "Vấn đề quan trọng 3",
                                    icon: "fa fa-file m--font-default"
                                }, {
                                    text: "Vấn đề quan trọng 4",
                                    icon: "fa fa-file m--font-danger"
                                }, {
                                    text: "Vấn đề quan trọng 5",
                                    icon: "fa fa-file m--font-info"
                                }]
                            }]
                        }, "Tài liệu khác"]
                    },
                    types: {
                        default: {
                            icon: "fa fa-folder m--font-success"
                        },
                        file: {
                            icon: "fa fa-file  m--font-success"
                        }
                    },
                    state: {
                        key: "demo2"
                    },
                    plugins: ["dnd", "state", "types"]
                }), $("#m_tree_6").jstree({
                    core: {
                        themes: {
                            responsive: !1
                        },
                        check_callback: !0,
                        data: {
                            url: function (e) {
                                return "https://keenthemes.com/metronic/preview/inc/api/jstree/ajax_data.php"
                            },
                            data: function (e) {
                                return {
                                    parent: e.id
                                }
                            }
                        }
                    },
                    types: {
                        default: {
                            icon: "fa fa-folder m--font-brand"
                        },
                        file: {
                            icon: "fa fa-file  m--font-brand"
                        }
                    },
                    state: {
                        key: "demo3"
                    },
                    plugins: ["dnd", "state", "types"]
                })
        }
    };
    jQuery(document).ready(function () {
        Treeview.init()
    });
}
catch (ex) {
    console.log('Error when config tree');
}


//Begin tranform
var ActiveDivTranform = 'tranform-lich';
function ActiveDiv(d1) {
    var item = document.getElementsByClassName("tranform-tacnghiep");
    //if (d1.getAttribute('idname') == ActiveDivTranform) {
    //    return;
    //}
    for (var i = 0; i < item.length; i++) {
        item[i].classList.remove('hide');
    }
    for (var i = 0; i < item.length; i++) {
        if (d1.getAttribute('idname') != item[i].id) {
            item[i].classList.add('hide');
        }
        item[i].setAttribute('style','transform: scale(0); backface-visibility:hidden;');
        item[i].classList.remove('hover');
    }
    for (var i = 0; i < item.length; i++) {
        if (d1.getAttribute('idname') == item[i].id) {
            item[i].setAttribute('style','transform: scale(-1);backface-visibility:unset;');

            item[i].classList.remove('hide');
            $(item[i]).toggleClass('hover');
            ActiveDivTranform = item[i].id;
            break;
        }
    }
    setTimeout(function () {
        for (var i = 0; i < item.length; i++) {
            if (d1.getAttribute('idname') != item[i].id) {
                item[i].setAttribute('style', 'transform: scale(0);backface-visibility:hidden;');
                item[i].classList.remove('hide');
            }
        }
    }, 100);
    for (var i = 0; i < item.length; i++) {
        if (d1.getAttribute('idname') == item[i].id) {
            var tem = document.getElementById('card-panel');
            tem.style.height = (item[i].clientHeight + 80) + 'px';
            break;
        }
    }
}
function inittranform() {
    var tem = $("[idname='" + ActiveDivTranform+"']")[0];
    ActiveDiv(tem);
    var item = document.getElementsByClassName("tranform-tacnghiep");
    for (var i = 0; i < item.length; i++) {
        if (ActiveDivTranform == item[i].id) {
            var tem = document.getElementById('card-panel');
            tem.style.height = (item[i].clientHeight + 80) + 'px';
            break;
        }
    }
}
setTimeout(inittranform, 2000);
//End tranform